# django-drf-utils

[![pipeline status](https://gitlab.com/biomedit/django-drf-utils/badges/main/pipeline.svg)](https://gitlab.com/biomedit/django-drf-utils/-/commits/main)
[![python version](https://img.shields.io/pypi/pyversions/django-drf-utils.svg)](https://pypi.org/project/django-drf-utils)
[![license](https://img.shields.io/badge/License-LGPLv3-blue.svg)](https://www.gnu.org/licenses/lgpl-3.0)
[![latest version](https://img.shields.io/pypi/v/django-drf-utils.svg)](https://pypi.org/project/django-drf-utils)
[![Ruff](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/astral-sh/ruff/main/assets/badge/v2.json)](https://github.com/astral-sh/ruff)

Utilities for commonly used functionality in `django` and `django-rest-framework`
among BioMedIT web services.
